/**
 * @vitest-environment happy-dom
 */

import { mount } from "@vue/test-utils";
import Form from "../components/Form.vue";
import { describe, expect, it } from "vitest";

describe("form.vue", () => {
  it("should render", () => {
    const wrapper = mount(Form);
    expect(wrapper.find('input[type="text"]').exists()).toBeTruthy();
    expect(wrapper.find('input[type="email"]').exists()).toBeTruthy();
    expect(wrapper.find("textarea").exists()).toBeTruthy();
  });

  it("validate fail firstname required ", async () => {
    const wrapper = mount(Form);

    await wrapper.find('input[type="text"]').setValue("");
    await wrapper.find('input[type="email"]').setValue("bagas@gmail.com");
    await wrapper.find("textarea").setValue("bagas note");
    await wrapper.find("button").trigger("click");

    expect(wrapper.find('div[class="error-msg"]').text()).toEqual(
      "Value is required"
    );
  });

  it("validate fail firstname strict name", async () => {
    const wrapper = mount(Form);

    await wrapper.find('input[type="text"]').setValue("bagas123");
    await wrapper.find('input[type="email"]').setValue("bagas@gmail.com");
    await wrapper.find("textarea").setValue("bagas note");
    await wrapper.find("button").trigger("click");

    expect(wrapper.find('div[class="error-msg"]').text()).toEqual(
      "Invalid Name. Valid name only contain letters, dashes (-) and spaces"
    );
  });

  it("validate fail firstname need more char ", async () => {
    const wrapper = mount(Form);

    await wrapper.find('input[type="text"]').setValue("as");
    await wrapper.find('input[type="email"]').setValue("bagas@gmail.com");
    await wrapper.find("textarea").setValue("bagas note");
    await wrapper.find("button").trigger("click");

    expect(wrapper.find('div[class="error-msg"]').text()).toEqual(
      "This field should be at least 3 characters long"
    );
  });

  it("validate fail email not an email", async () => {
    const wrapper = mount(Form);

    await wrapper.find('input[type="text"]').setValue("bagas");
    await wrapper.find('input[type="email"]').setValue("bagas");
    await wrapper.find("textarea").setValue("bagas note");
    await wrapper.find("button").trigger("click");

    expect(wrapper.find('div[class="error-msg"]').text()).toEqual(
      "Value is not a valid email address"
    );
  });

  it("validate fail email required ", async () => {
    const wrapper = mount(Form);

    await wrapper.find('input[type="text"]').setValue("bagas");
    await wrapper.find('input[type="email"]').setValue("");
    await wrapper.find("textarea").setValue("bagas note");
    await wrapper.find("button").trigger("click");

    expect(wrapper.find('div[class="error-msg"]').text()).toEqual(
      "Value is required"
    );
  });

  it("validate fail note required ", async () => {
    const wrapper = mount(Form);

    await wrapper.find('input[type="text"]').setValue("bagas");
    await wrapper.find('input[type="email"]').setValue("bagas@gmail.com");
    await wrapper.find("textarea").setValue("");
    await wrapper.find("button").trigger("click");

    expect(wrapper.find('div[class="error-msg"]').text()).toEqual(
      "Value is required"
    );
  });

});
